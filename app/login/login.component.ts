import { Router } from "@angular/router";
import { Component } from "@angular/core";
@Component({
    selector: "gr-login",

    styleUrls: ["login/login.component.css"],
    templateUrl: "login/login.component.html"
})

export class LoginComponent {


    constructor(private router: Router) {

    }



    gonext() {
        this.router.navigateByUrl('/page2');

    }
}

    