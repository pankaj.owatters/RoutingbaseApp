import { LoginComponent } from "./login/login.component";
import { ListComponent } from "./list/list.component";

export const routes = [
    { path: "", component: LoginComponent },
    { path: "page2", component: ListComponent },
    { path: "page1", component: LoginComponent }
];

export const navigatableComponents = [
    LoginComponent,
    ListComponent
];
