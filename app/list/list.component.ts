import { Component } from "@angular/core";
import { Router } from "@angular/router";
@Component({
    selector: "gr-list",
    templateUrl: "list/list.component.html",
    styleUrls: ["list/list.component.css"]
})
export class ListComponent { 
    constructor(private router: Router) {
       
    }
    goback() {
        this.router.navigateByUrl('/page1');
    }
}